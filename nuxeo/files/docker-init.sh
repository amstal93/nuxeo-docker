#!/bin/bash 
set -e

# this script is run under the nuxeo user id
cd $NUXEO_HOME

NUXEO_MPINSTALL_OPTIONS="${NUXEO_MPINSTALL_OPTIONS:---relax=false}"

if [ "$1" = 'nuxeoctl' ]; then


  for f in /docker-entrypoint-initnuxeo.d/*; do
    case "$f" in
      *.sh)  echo "$0: running $f"; . "$f" ;;
      *.zip) echo "$0: installing Nuxeo package $f"; $NUXEO_HOME/bin/nuxeoctl mp-install $f ${NUXEO_MPINSTALL_OPTIONS} --accept=true ;;
      instance.clid) echo "$0: moving clid to $NUXEO_DATA"; mv $f $NUXEO_DATA/instance.clid ;;      
      nuxeo.j\?conf)  ;;
      *)   echo "ignoring unexpected file $0" ;;
    esac
  done

  # instance.clid
  if [ -n "$NUXEO_CLID" ]; then
    # Replace --  by a carriage return
    NUXEO_CLID="${NUXEO_CLID/--/\\n}"
    printf "%b\n" "$NUXEO_CLID" >> $NUXEO_DATA/instance.clid
  fi

  ## Executed at each start
  if [ -n "$NUXEO_CLID"  ] && [ ${NUXEO_INSTALL_HOTFIX:='true'} == "true" ]; then
      $NUXEO_HOME/bin/nuxeoctl mp-hotfix --accept=true
  fi

  # Install packages if exist
  if [ -n "$NUXEO_PACKAGES" ]; then
    $NUXEO_HOME/bin/nuxeoctl mp-install $NUXEO_PACKAGES $NUXEO_MPINSTALL_OPTIONS --accept=true
  fi

  if [ "$2" = "console" ]; then
    [ -z "$NUXEO_URL" ] && echo "Environment variable NUXEO_URL is required but not set" && exit 100
    exec $NUXEO_HOME/bin/nuxeoctl console
  else
    exec "$@"
  fi

fi

exec "$@"
