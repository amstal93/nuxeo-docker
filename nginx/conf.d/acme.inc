location /.well-known/acme-challenge {
    alias /var/www/acme;

    location ~ /.well-known/acme-challenge/(.*) {
      add_header Content-Type application/jose+json;
    }
}
