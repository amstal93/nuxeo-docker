#!/bin/sh

# Obtain SSL Certifcates from LetsEncrypt: 
# This script stands up a temporary server just for the purpose of obtaining certificates in the docker volume
# 1. edut nginx/nginx-setup.conf and change YOURHOSTNAME to the server name, e.g., www.example.com
# 2. uncomment the first FLAGS line below and comment the second one. This will use the staging server at LetsEncrypt. Use this setting to debug your configuration. If you make too many calls to LetsEncrypt they can blacklist your site for a week.
# 3. Set HTTP_LISTEN_PORT to the external HTTP port of your server or VM. If your server is an instance on Google Compute Engine, this must be 8080: that's the only HTTP port you can listen on as 80 is already occupied. A GCE load balancer (listening on port 80 on a public IP) needs to direct incoming HTTP port 80 traffic to your virtual server on port 8080. If you're not on GCE, this will probably be 80 (if the server is directly on the internet) or 8080 (if behind a load balancer).
# 4. Run and watch the logs to make sure you get successful certificate generation from LetsEncrypt. If you're not sure that port 80 is really exposed, try running bin/pingserver.sh and then running "curl http://myserver.com/_ping" - you should get a response "pong".
# 5. Switch the FLAGS line and re-run. 

if [ ! -d ./nginx ]; then
  echo "starting from wrong directory"
  exit 2
fi

HTTP_LISTEN_PORT=8080

# uncomment one of these two FLAGS settings
# use first one for testing, second one for production
FLAGS="-e USE_STAGING=1"
#FLAGS="-v nuxeodocker_certs:/etc/ssl/acme"

# there's a 20-second sleep time in here to make sure the health check
# has a chance to succeed, and the load balancer starts sending traffic to it
docker run -it  \
    -p $HTTP_LISTEN_PORT:80 \
    $FLAGS \
    -e "DOMAIN_LIST=/etc/nginx/conf.d/domains.txt"  \
    -v "$PWD/nginx/conf.d:/etc/nginx/conf.d" \
    -v "$PWD/nginx/nginx-setup.conf:/etc/nginx/nginx.conf" \
    nuxeodocker_nginx sh -c "nginx; sleep 20; /etc/periodic/weekly/acme-client"

