#!/bin/sh

MYUID=$(id -u)
MYGID=$(id -g)
docker run --rm -it -v $PWD:/backup -v nuxeodocker_certs:/data alpine:latest sh -c \
   "cd /data; tar -cf /backup/certs.tar * && chown $MYUID:$MYGID /backup/certs.tar"

