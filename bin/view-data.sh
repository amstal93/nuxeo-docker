#!/bin/sh
# mount all volumes readonly for viewing/backup
# use -rw to mount writable (be careful!). Also make sure if files are added/changed they have appropriate permissions
READFLAG=ro
if [ "x$1" = "x-rw" ]; then
    READFLAG=rw
    shift
fi
NUXEO_VERSION=${NUXEO_VERSION:-9.2}
for v in $(docker volume ls | grep nuxeodocker_ | sed s/^.*nuxeodocker_// ); do
    VARGS="$VARGS -v nuxeodocker_${v}:/mnt/${v}:${READFLAG}"
done
if [ "x$VARGS" = "x" ]; then
    echo No volumes yet - containers need to run first to generate data
    exit
fi
docker run --rm -it --user=root $VARGS \
    --entrypoint=/bin/bash \
    -w /mnt \
    stevelr/nuxeo:ubuntu-${NUXEO_VERSION} $@
