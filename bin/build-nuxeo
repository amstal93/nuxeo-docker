#!/bin/bash
#
# Build nuxeo docker image based on downloaded release (or snapshot or staging) zip file from nuxeo.com
#    see urls below for paths to relase folders
#
#    (file name will be of the form nuxeo-server-VERSION-tomcat.zip )
#

set -e
#BTYPE=alpine
BTYPE=ubuntu
FORCE=n

showhelp() {
cat <<EOF
   Syntax:
      bin/build-nuxeo [-a] VERSION

   Use -a to build the alpine-based image
   Otherwise it will build ubuntu-based image

   VERSION should be a simple version such as 9.1
      or a snapshot version such as one of the following:
EOF
bin/list-releases
}

[ -d ./nuxeo ] & [ -d ./nuxeo-pre ] || ( echo "Please run this from the nuxeo-docker directory" && exit 1 )


while :; do
    case $1 in
      -h|--help)
          showhelp
          exit
          ;;
      -a|--alpine)
          BTYPE="alpine"
          ;;
      -f|--force)
          FORCE="y" 
          ;;
      --release)
          REPO=release
          ;;
      --staging)i
          REPO=staging
          ;;
      --snapshot)
          REPO=snapshot
          ;;
      -?*)
          printf 'ERROR: Unknown option: %s\n' "$1" >&2
          showhelp
          exit 2
          ;;
      *)    # no more options, break out of loop
          break;
          ;;
    esac
    shift
done
NUXEO_VERSION="$1"
[ -z "$NUXEO_VERSION" ] && showhelp && exit 2

RELEASE_CACHE=./releases
BASE_NAME=nuxeo
BASE_VER=${BTYPE}-${NUXEO_VERSION}

get_release() {
  VER=$1
  FILE=nuxeo-server-${VER}-tomcat.zip

  mkdir -p $RELEASE_CACHE

  # try cache
  if [ -f ${RELEASE_CACHE}/$FILE ]; then
     if [ "$FORCE" = "y" ]; then
         echo "Ignoring cached file $FILE and checking for updates ..."
     else
         echo "Found cached version" && return 0
     fi
  fi

  # try release
  if [ -z "$REPO" ] || [ "$REPO" = "release" ]; then
      echo Attempting to download release $VER 
      curl -f -L -o $RELEASE_CACHE/$FILE "https://community.nuxeo.com/static/releases/nuxeo-${VER}/${FILE}"
      [ $? -eq 0 ] && echo "Download complete" && return 0
  fi

  # try staging
  if [ -z "$REPO" ] || [ "$REPO" = "staging" ]; then
      echo Attempting to download staging $VER
      curl -f -L -o $RELEASE_CACHE/$FILE "https://community.nuxeo.com/static/staging/${FILE}"
      [ $? -eq 0 ] && echo "Download complete" && return 0
  fi

  # try snapshot
  if [ -z "$REPO" ] || [ "$REPO" = "snapshot" ]; then
      echo Attempting to download snapshot $VER

      # snapshot format x.x-Iyyyymmdd_nnn
      curl -f -L -o $RELEASE_CACHE/$FILE "https://community.nuxeo.com/static/snapshots/${FILE}"
      [ $? -eq 0 ] && echo "Download complete" && return 0

      # alternate file name format x.x-SNAPSHOT
      F2=nuxeo-server-tomcat-${VER}.zip
      curl -f -L -o $RELEASE_CACHE/$FILE "https://community.nuxeo.com/static/snapshots/${F2}"
      [ $? -eq 0 ] && echo "Download complete" && return 0
  fi

  # fail
  echo ERROR: Nuxeo version $VER not found
  FILE=""
  return 1
}

# ensure pre-requisite build
PRE_IMAGE_NAME=nuxeo
#PRE_IMAGE_VER=0.1-$(date +%y%m%d%H%M)
PRE_IMAGE_VER=pre-${BTYPE}
echo Building intermediate container ${PRE_IMAGE_NAME}:${PRE_IMAGE_VER}
docker build -t ${PRE_IMAGE_NAME}:${PRE_IMAGE_VER} \
    -f nuxeo-pre/Dockerfile-${BTYPE} \
    nuxeo-pre
if [ $? -ne 0 ]; then
    echo Build intermediate image ${PRE_IMAGE_NAME} failed
    exit
fi
if [ "$PRE_IMAGE_VER" != "latest" ]; then
    docker tag ${PRE_IMAGE_NAME}:${PRE_IMAGE_VER} ${PRE_IMAGE_NAME}:latest
fi

echo Building Nuxeo docker container for Version $NUXEO_VERSION.

# use cached release zip or fetch release
get_release $NUXEO_VERSION || ( showhelp && exit 3 )

# make available to docker build
mkdir -p ./nuxeo/dist
cp ${RELEASE_CACHE}/$FILE ./nuxeo/dist/

docker build -t ${BASE_NAME}:${BASE_VER} \
    --build-arg "NUXEO_VERSION=${NUXEO_VERSION}" \
    --build-arg "NUXEO_PRE_IMAGE=${PRE_IMAGE_NAME}" \
    --build-arg "NUXEO_PRE_VERSION=${PRE_IMAGE_VER}" \
    --label="my.nuxeo.version=${NUXEO_VERSION}" nuxeo

echo Remember to update the image version in docker-compose.yml, currently set to:
grep "image: nuxeo-" docker-compose.yml

# clear docker copy but keep in cache
rm -f ./nuxeo/dist/$FILE
