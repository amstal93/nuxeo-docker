#!/bin/sh
#
# This is a really simple http server that responds to the url "/_ping"
# and returns "pong". It's useful to check http connectivity to the server

if [ ! -d ./nginx ]; then
  echo "starting from wrong directory"
  exit 2
fi

docker run -d --rm \
    -p 8080:80 \
    -e "USE_STAGING=1"  \
    -e "DOMAIN_LIST=/etc/nginx/conf.d/domains.txt"  \
    -v "$PWD/nginx/conf.d:/etc/nginx/conf.d" \
    -v "$PWD/nginx/nginx-ping.conf:/etc/nginx/nginx.conf" \
    nuxeodocker_nginx

