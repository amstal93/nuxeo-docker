# Full-stack docker cloud for Nuxeo Document Management System

### Features:
* Builds a full-stack Nuxeo cloud with containers for Nuxeo CMS, Postgres, Redis, Elasticsearch, and Nginx
* Uses recent versions of Postgres (9.6), Redis (3.2), Nginx (1.13), and the latest version of Elasticsearch supported by nuxeo.
* SSL certificates from Lets Encrypt
* Sticky session load balancing (using tomcat's JSESSIONID cookie)
* Supports localhost development mode and production use
* Simple nuxeo add-on package installation and configuration management
* Persistent docker volumes for database, SSL certificates, logs, and package add-ons.
* nginx configuration templates to simplify multi-host configuration and dynamic load balancing
* Development environment tweaks:
  * intermediate docker container shared by multiple nuxeo versions
  * nuxeo release zip file downloaded once and cached in releases/ dir
  * config files processed with jinja2 template engine for added flexibility and control
  * several utility scripts (check out bin/ directory)
* Security:
  * simplify changing default Administrator before service goes online
  * SSL configuration out-of-the-box gets "A" rating by [Qualys](https://ssllabs.com/)

### Experimental features:
 * alpine-based build, about 600MB smaller than ubuntu-based build. To try the alpine-based build, add a `-a` parameter to build-nuxeo before the VERSION.

# Quick Start (for localhost development)

```
  # Check out the entire project to get default configuration files
  git clone https://gitlab.com/stevelr/nuxeo-docker.git
  cd nuxeo-docker
```
The default configuration is bare-bones and has no content. If you are new to Nuxeo and want to see the demo content for "Big Corp" and template samples that are used in the [Documentation](https://doc.nuxeo.com) and tutorials on [Nuxeo University](https://university.nuxeo.com), edit nuxeo/setup.env and select the second version of NUXEO_PACKAGES that contains the two extra packages.
```
  # Start all servers
  docker-compose up -d
  # To view logs
  docker-compose logs -f
```
Open your browser to http://localhost:8080/ and log in with Administrator/Administrator. On the first run of bin/startup, expect about 1.9 GB of docker images to download, and another 15-30 seconds for first-time staratup.

# Getting Started (dev or production)

### 1. Pull or build docker images for the desired version of nuxeo

```
  # fetch all necessary docker images
  docker-compose pull

  # If you want to build your own nuxeo container you can use the build-nuxeo script
  bin/build-nuxeo VERSION
```
For custom builds, VERSION may be 8.10 (the latest LTS release), 9.1, 9.2, 9.3-SNAPSHOT, or one of the nightly builds. To see which nuxeo releases are available, run `bin/list-releases.sh`.

Custom builds will generate an intermediate docker image called nuxeo:pre-ubuntu and a final image called nuxeo:ununtu-VERSION.
_If you generate a custom build, change the nuxeo image name in docker-compose.yml._

### 2. Update config files

#### _For Development_
The default configuration is already set up for development and use on localhost.

* To use a custom port other than 8080, change both occurrences in docker-compose.yml.
* Postgres, Redis, and Elastic Search servers don't expose their ports to the docker host. To temporarily expose one of the ports for testing, you can run `bin/proxy SERVICE [PORT]` (where SERVICE is db,redis, or es)

#### _For Production_
 * _For the instructions below, replace `cms.example.com` with your production host and domain name._
 - in bin/startup, set NGINX_CONFIG_MODE=prod
 - in docker-compose.yml,
    - set nginx ports to be "80:80" and "443:443". Even if the production server will only support https, port 80 is needed for negotiating new SSL certs from LetsEncrypt.
    - set NUXEO_URL to https://cms.example.com (replace with real host/domain)
 - nginx/conf.d/domains.txt should contain the fully qualified server name e.g., `cms.example.com`. If the server is listening on other hostnames (for multi-tenacy, for example), add those as separate lines
 - in nuxeo/setup.conf
   - generate new random strings for SERVER_STATUS_KEY and SERVER_KEYSTORE_KEY
   - See https://doc.nuxeo.com/nxdoc/configuration-parameters-index-nuxeoconf/ for more information
 - Decide how many nuxeo tomcat containers there will be (typically 1 or 2).
 - To run one nuxeo container only:
     - ensure only one name (nuxeo1) is uncommented in nginx/conf.d/backends.txt
     - comment out links:nuxeo2 in nginx config in docker-compose.yml
 - To run two or more nuxeo servers:
     - include each server name in a separate line in nginx/conf.d/backends.txt (nuxeo1,nuxeo2,...). The names do not need to be in the format "nuxeo"+NN but it's a helpful convention, and if the NN suffix matches the NUXEO_CLUSTER_ID, it will be easier to read the log files.
     - replicate nuxeo setup in docker-compose.yml so there is one server per nuxeo instance
     - ensure that each nuxeo backend in docker-compose.yml has a unique NUXEO_CLUSTER_ID
     - in docker-compose.yml, ensure that all nuxeo service names (nuxeo1,nuxeo2, ...) appear in the nginx links section
    
### 3. Install jinja2 templates
You'll need python (2 or 3) and the jinja2 package to run the nginx templates
```
  pip install jinja2
```

### 4. Initialize servers, admin password, and SSL certs

Start the servers. On first run, this will start all the servers except nginx.
```
  bin/startup
```
Change the default password
```
  bin/set-admin-password SECUREPASSWORD
```
If you need SSL certs, run a temporary nginx container to get them
```
  docker-compose build nginx
  docker run --rm -p 8080:80 --cap-add NET_BIND_SERVICE \
       -e USE_STAGING:1 \
       -e DOMAIN_LIST:/etc/nginx/conf.d/domains.txt \
       -v $PWD/nginx/nginx-setup.conf:/etc/nginx/nginx.conf  \
       nuxeodocker_nginx /etc/periodic/weekly/acme-client
```
The USE_STAGING:1 flag causes the cert request to go to Lets Encrypt's Staging servers
and the new certicate will work but will cause browser warnings. Let's Encrypt blocks you if you make too many requests
to their production servers, so It's best to test requests against their staging until you know
everything is debugged and working. When you see no errors from the above command, repeat
without staging:
```
  docker run --rm -p 80:80 --cap-add NET_BIND_SERVICE \
       -e DOMAIN_LIST:/etc/nginx/conf.d/domains.txt \
       -v nuxeodocker_certs:/etc/ssl/acme \
       -v $PWD/nginx/nginx-setup.conf:/etc/nginx/nginx.conf  \
       nuxeodocker_nginx /etc/periodic/weekly/acme-client
```
Now the certs are saved in the docker volume nuxeodocker_certs, and you can run the regular nginx container to use them.
```
  docker-compose up -d nginx
```
Finally, clear the first-time flag so that next time you run bin/startup it will start everything
```
  rm bin/CHANGE-DEFAULT-PASS
```
View the web UI at http://localhost:8080 (for dev mode) or https://cms.example.com (for prod).

### 5. Typical start & stop

After the servers have been initialized with the procedure above, you can use standard docker commands. To start all servers
```
    docker-compose up -d
```
to stop all servers
```
    docker-compose down
```
